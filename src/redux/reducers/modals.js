export const createTagModalReducer = (state = false, action) => {
  switch (action.type) {
    case 'OPEN_CREATE_TAG_MODAL':
      return true;
    case 'CLOSE_CREATE_TAG_MODAL':
      return false;
    default:
      return state;
  }
};

export const editTagModalReducer = (state = false, action) => {
  switch (action.type) {
    case 'OPEN_EDIT_TAG_MODAL':
      return true;
    case 'CLOSE_EDIT_TAG_MODAL':
      return false;
    default:
      return state;
  }
};
