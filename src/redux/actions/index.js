import axios from 'axios';
import config from '../../Config';

export const fetchTransactions = (filter) => {
  return {
    type: 'FETCH_TRANSACTIONS',
    payload: filter,
  };
};

export const fetchTags = (offset, limit) => {
  return {
    type: 'FETCH_TAGS',
    payload: {
      offset,
      limit,
    },
  };
};

export const setTrasactionsFilter = (filter) => {
  return {
    type: 'SET_TRANSACTIONS_FILTER',
    payload: filter,
  };
};

export const openCreateTagModal = () => {
  return {
    type: 'OPEN_CREATE_TAG_MODAL',
  };
};

export const closeCreateTagModal = () => {
  return {
    type: 'HIDE_CREATE_TAG_MODAL',
  };
};

export const openEditTagModal = () => {
  return {
    type: 'OPEN_EDIT_TAG_MODAL',
  };
};

export const hideEditTagModal = () => {
  return {
    type: 'HIDE_EDIT_TAG_MODAL',
  };
};
