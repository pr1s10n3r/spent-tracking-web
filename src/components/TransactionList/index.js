import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { openTransactionEditModal } from '../../redux/actions/transaction.action';
import * as moment from 'moment';

import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import Table from 'react-bootstrap/Table';

function TransactionList() {
  const dispatch = useDispatch();
  const state = useSelector((state) => state.transactions);

  const handleEditTransaction = (transaction) => {
    dispatch(openTransactionEditModal(transaction));
  };

  const formatDate = (date) => moment(date).format('DD-MM-YYYY');
  const formatMoney = (value) => {
    const formatter = new Intl.NumberFormat('es-CO', {
      style: 'currency',
      currency: 'COP',
    });

    const formated = formatter.format(value);

    if (value > 0) {
      return <td className="text-success">{formated}</td>;
    } else if (value === 0) {
      return <td className="text-white">{formated}</td>;
    }
    return <td className="text-danger">{formated}</td>;
  };

  const getTransactionRow = (transaction, index) => (
    <tr key={index} onClick={() => handleEditTransaction(transaction)}>
      {formatMoney(transaction.value)}
      <td>{transaction.tag.name}</td>
      <td>{formatDate(transaction.created_at)}</td>
      <td>{formatDate(transaction.updated_at)}</td>
    </tr>
  );

  if (!state.isLoading) {
    return (
      <div>
        <Table responsive striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>Monto</th>
              <th>Etiqueta</th>
              <th>Fecha de Creación</th>
              <th>Última Actualización</th>
            </tr>
          </thead>
          {
            <tbody>
              {state.transactions.map((tx, i) => getTransactionRow(tx, i))}
            </tbody>
          }
        </Table>
      </div>
    );
  }

  return (
    <Spinner animation="border" role="status">
      <span className="sr-only">Cargando transacciones...</span>
    </Spinner>
  );
}

export default TransactionList;
