import React from 'react';

import Alert from 'react-bootstrap/Alert';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import { useSelector, useDispatch } from 'react-redux';
import {
  hideCreateTransactionModal,
  setEditingTransactionValue,
  setEditingTransactionTag,
  saveTransaction,
} from '../../redux/actions/transaction.action';

function CreateTransaction() {
  const dispatch = useDispatch();
  const txState = useSelector((state) => state.transactions);
  const tagState = useSelector((state) => state.tags);

  const onCreateHide = () => {
    dispatch(hideCreateTransactionModal());
  };

  const handleCreateTransaction = () => {
    if (!txState.editing.tag) {
      dispatch(setEditingTransactionTag(tagState.tags[0].id));
    }
    dispatch(saveTransaction(txState.editing.tag.id, txState.editing.value));
  };

  const onTrasanctionValueChange = (event) => {
    if (!isNaN(event.target.value)) {
      let value = +event.target.value;

      if (txState.isDeposit) {
        value = Math.abs(value);
      } else {
        if (value > 0) {
          value *= -1;
        }
      }

      dispatch(setEditingTransactionValue(value));
    }
  };

  const onTransactionTagChange = (event) => {
    const tagId = +event.target.value;
    dispatch(setEditingTransactionTag(tagId));
  };

  const getAlertResult = () => {
    if (txState.success) {
      if (txState.success) {
        return (
          <Alert variant="success">
            ¡Transacción creada satisfactoriamente!
          </Alert>
        );
      }

      return <Alert variant="danger">¡No se pudo crear la transacción!</Alert>;
    }

    return null;
  };

  return (
    <Modal
      onHide={onCreateHide}
      show={txState.showCreate}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {txState.isDeposit ? 'Nuevo Ingreso' : 'Nuevo Egreso'}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Group controlId="formBasicEmail">
              <Form.Label>
                {txState.isDeposit
                  ? '¿Cuánto dinero ingresó?'
                  : '¿Cuánto dinero usaste?'}
              </Form.Label>
              <Form.Control
                onChange={onTrasanctionValueChange}
                type="number"
                defaultValue={0.0}
              />
              <Form.Text className="text-muted">
                Ingresas la cantidad en pesos colombianos.
              </Form.Text>
            </Form.Group>

            <Form.Group>
              <Form.Label>
                {txState.isDeposit
                  ? '¿De dónde vino ese dinero?'
                  : '¿En qué usaste el dinero?'}
              </Form.Label>
              <Form.Control as="select" onChange={onTransactionTagChange}>
                {tagState.tags.map((tag, index) => (
                  <option key={index} value={tag.id}>
                    {tag.name}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>
          </Form.Group>
        </Form>
        {getAlertResult()}
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onCreateHide}>Cerrar</Button>
        <Button variant="success" onClick={handleCreateTransaction}>
          Guardar
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default CreateTransaction;
