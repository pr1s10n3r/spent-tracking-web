const devConfig = {
  api: {
    URL: 'http://127.0.0.1:8080/api',
  },
};

const prodConfig = {
  api: {
    URL: 'http://127.0.0.1:8080/api',
  },
};

const config = process.env.REACT_PRODUCTION ? prodConfig : devConfig;

export default config;
